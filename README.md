# ABOUT

LaTeX Class File : `bibleref-arabic`

Author        : John R. Supplee

This package is based on the `bibleref` package by Nicola Talbot and Maïeul Rouquette.  It has been modified to work with 

Author        : Nicola Talbot (inactive) and Maïeul Rouquette (still active)

Files         : bibleref.dtx   - documented source file
                bibleref.ins   - installation script

LPPL 1.3  https://www.ctan.org/license/lppl1.3

https://bitbucket.org/jrsupplee/bibleref-arabic

Feature requests: https://framagit.org/maieul/bibleref/issues


The package file `bibleref-arabic.sty` can be used to ensure consistent formatting of bible citations.  Also comes with `bibleref-arabic-xidx.sty` for extended bibleref-arabic indexing functions.

# INSTALLATION

    make install

Put `bibleref-arabic` somewhere on TeX's path and refresh the database.


A sample file, sample-arabic.tex, is provided.


# REQUIREMENTS

* `ifthen`

* `fmtcount`

* `amsgen`

This material is subject to the LaTeX Project Public License.

See http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for the details of that license.
