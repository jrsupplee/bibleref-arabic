.PHONY: install


LOCAL_DIR=$(shell kpsewhich -var-value=TEXMFHOME)/tex/latex/

install:
	install -D -m 664 bibleref-arabic.sty $(LOCAL_DIR)/bibleref-arabic.sty
	install -m 664 bibleref-arabic-xidx.sty $(LOCAL_DIR)
